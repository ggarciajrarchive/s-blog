package br.com.scalado.sblog.snippet

import net.liftweb._
import common._
import util.Helpers._
import util.Props
import net.liftweb.common.{Empty, Box}
import net.liftweb.http.js.JsCmd
import net.liftweb.http.{SHtml, S, FileParamHolder}
import br.com.scalado.sblog.lib.FileUpload
import br.com.scalado.sblog.model._
import br.com.scalado.sblog.lib.Sha1Hasher
import br.com.scalado.sblog.model.Blog

/**
 * Created by Eureka.
 * User: ggarcia
 * Date: 2/16/12
 * Time: 10:01 PM
 *
 * @author ggarcia
 */
object Install {
  def form = {
    /* blog's info */
    var blogName = ""
    var blogDescr = ""

    /* author's info */
    var authorName = ""
    var authorEmail = ""
    var authorPass = ""
    var authorResume = ""

    val folder = Props.get("folders.pics") openOr ""
    var picHolder: Box[FileParamHolder] = Empty

    def process(): JsCmd = {
      val picName = FileUpload.processFile(folder, picHolder)
      println(picHolder)
      println(picName)

      val author = Author.createRecord
        .name(authorName)
        .email(authorEmail)
        .password(Sha1Hasher.hash(authorPass))
        .picture(picName)
        .resume(authorResume)

      val blog = Blog.createRecord
        .name(blogName)
        .description(blogDescr)

      author.save
      blog.save

      S.redirectTo("/")
    }

    /**
     * CSS Selectors
     */
    "id=blogName" #> SHtml.text(blogName, blogName = _) &
      "id=blogDescr" #> SHtml.textarea(blogDescr, blogDescr = _) &
      "id=authorName" #> SHtml.text(authorName, authorName = _) &
      "id=authorEmail" #> SHtml.text(authorEmail, authorEmail = _) &
      "id=authorPass" #> SHtml.password(authorPass, authorPass = _) &
      "id=authorResume" #> SHtml.textarea(authorResume, authorResume = _) &
      "id=authorPic" #> (SHtml.fileUpload((fph) => picHolder = Full(fph))) &
      ":submit" #> SHtml.submit("Salvar", process)
  }
}
