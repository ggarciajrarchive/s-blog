package br.com.scalado.sblog.model

import net.liftweb.record.field.StringField
import net.liftweb.mongodb.record.{MongoMetaRecord, MongoRecord}
import math.BigInt._
import net.liftweb.json.JsonAST.{JInt, JField, JObject}
import net.liftweb.mongodb.ObjectIdSerializer
import net.liftweb.mongodb.record.field.IntPk

/**
 * Created by Eureka.
 * User: ggarcia
 * Date: 2/16/12
 * Time: 10:04 PM
 *
 * @author ggarcia
 */

/* Author */
class Author extends MongoRecord[Author] with IntPk[Author] {
  def meta = Author

  object name extends StringField(this, 200)

  object email extends StringField(this, 256)

  object password extends StringField(this, 40)

  object picture extends StringField(this, 100)

  object resume extends StringField(this, 300)

}

object Author extends Author with MongoMetaRecord[Author] {
  override def collectionName = "author"

  override def formats = super.formats + new ObjectIdSerializer

  this.ensureIndex(
    new JObject(
      JField("name", JInt(1)) :: Nil
    )
  )
}

/* Blog */
class Blog extends MongoRecord[Blog] with IntPk[Blog] {
  def meta = Blog

  object name extends StringField(this, 200)

  object description extends StringField(this, 300)

}

object Blog extends Blog with MongoMetaRecord[Blog] {
  override def collectionName = "blog"

  override def formats = super.formats + new ObjectIdSerializer

  this.ensureIndex(
    new JObject(
      JField("name", JInt(1)) :: Nil
    )
  )
}
