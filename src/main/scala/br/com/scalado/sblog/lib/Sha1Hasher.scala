package br.com.scalado.sblog.lib

import java.security.MessageDigest

/**
 * Created by Eureka.
 * User: ggarcia
 * Date: 2/16/12
 * Time: 10:18 PM
 *
 * @author ggarcia
 */
object Sha1Hasher {
  private val chars =
    Map(
      0 -> '0', 1 -> '1', 2 -> '2', 3 -> '3', 4 -> '4', 5 -> '5',
      6 -> '6', 7 -> '7', 8 -> '8', 9 -> '9', 10 -> 'a', 11 -> 'b',
      12 -> 'c', 13 -> 'd', 14 -> 'e', 15 -> 'f'
    );

  private def convertToHex(data: Array[Byte]): String = {
    val buf = new StringBuilder();
    for (b <- data) {
      buf.append(chars(b >>> 4 & 0x0F));
      buf.append(chars(b & 0x0F));
    }
    buf.toString();
  }

  def hash(text: String): String = {
    val md = MessageDigest.getInstance("SHA-1");
    md.update(text.getBytes("UTF-8"), 0, text.length());
    convertToHex(md.digest());
  }
}
