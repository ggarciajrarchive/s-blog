package br.com.scalado.sblog.lib

/**
 * Created by Eureka.
 * User: ggarcia
 * Date: 4/3/12
 * Time: 10:59 PM
 *
 * @author ggarcia
 */
import net.liftweb._
import mongodb._
import util.Props
import com.mongodb.{Mongo, ServerAddress}

object SBlogDB extends MongoIdentifier {
  val jndiName = "SBlogDB"
}

object MongoConfig {
  def init() {
    val server = new ServerAddress(
      Props.get("mongo.host", "localhost"),
      Props.getInt("mongo.port", 27017)
    )
    MongoDB.defineDb(DefaultMongoIdentifier, new Mongo(server), "sblog")
    MongoDB.defineDb(SBlogDB, new Mongo(server), "sblog")
  }
}