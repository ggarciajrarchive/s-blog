package br.com.scalado.sblog.lib

import net.liftweb.common.Box
import net.liftweb.http.FileParamHolder
import java.util.Calendar
import java.io.{FileOutputStream, InputStream, File}

/**
 * Created by Eureka.
 * User: ggarcia
 * Date: 2/16/12
 * Time: 10:03 PM
 *
 * @author ggarcia
 */
object FileUpload {
  def processFile(folder: String, file: Box[FileParamHolder]): String = {
    var pictureName = ""

    file.map {
      f => {
        pictureName =
          Calendar.getInstance().getTimeInMillis.toString + "_" + f.fileName

        val file = new File(folder + "/" + pictureName)

        inputToFile(f.fileStream, file)
      }
    }

    pictureName
  }

  private def inputToFile(is: InputStream, f: File) {
    val out = new FileOutputStream(f)

    Iterator
      .continually(is.read)
      .takeWhile(-1 !=)
      .foreach(out.write)
  }
}
