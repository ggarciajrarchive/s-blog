package bootstrap.liftweb

import net.liftweb._
import http._
import sitemap.{SiteMap, Menu, Loc}
import util.{NamedPF}
import net.liftweb._
import common.{Full}
import br.com.scalado.sblog.model._
import _root_.net.liftweb.sitemap.Loc._
import br.com.scalado.sblog.lib.MongoConfig

class Boot {
  def boot {

    // database access
    MongoConfig.init()

    // where to search snippet
    LiftRules.addToPackages("br.com.scalado.sblog")

    // protect restricted areas
    val isNotInstalled = If(() => {
      Blog.findAll.isEmpty
    }, () => S.redirectTo("/"))

    // build sitemap
    val entries = (List(Menu("Home") / "index") :::
      // the User management menu items
      User.sitemap :::
      List(Menu("Instalação") / "install" >> isNotInstalled) :::
      List(Menu(Loc("Static", Link(List("static"), true, "/static/index"), "Static Content"))) :::
      Nil)

    LiftRules.uriNotFound.prepend(NamedPF("404handler") {
      case (req, failure) => NotFoundAsTemplate(
        ParsePath(List("exceptions", "404"), "html", false, false))
    })

    LiftRules.setSiteMap(SiteMap(entries: _*))

    // set character encoding
    LiftRules.early.append(_.setCharacterEncoding("UTF-8"))

    //Show the spinny image when an Ajax call starts
    LiftRules.ajaxStart =
      Full(() => LiftRules.jsArtifacts.show("ajax-loader").cmd)

    // Make the spinny image go away when it ends
    LiftRules.ajaxEnd =
      Full(() => LiftRules.jsArtifacts.hide("ajax-loader").cmd)

    // Use HTML5 for rendering
    LiftRules.htmlProperties.default.set((r: Req) =>
      new Html5Properties(r.userAgent))
  }
}